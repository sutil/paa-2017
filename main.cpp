using namespace std;

#include <string>
#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <unistd.h>

int simples(vector<int> vetor, int tamanho, int numeroProcurado){
    int numeroDeVezesQueAparece = 0;

    for(int i = 0; i< tamanho; i++)
        if(vetor[i] == numeroProcurado) {
            usleep(1);
            numeroDeVezesQueAparece++;
        }

    return numeroDeVezesQueAparece;
}

int buscaIndiceInicio(vector<int> vetor, int numeroProcurado, int inicio, int fim){
    if(inicio == fim){
        if(numeroProcurado == vetor[inicio]) {
            usleep(1);
            return inicio;
        }
        else
            return -1;
    }

    int meio = (inicio + fim) / 2;

    if(numeroProcurado <= vetor[meio]){
        return buscaIndiceInicio(vetor, numeroProcurado, inicio, meio);
    } else
        return buscaIndiceInicio(vetor, numeroProcurado, meio + 1, fim);
}

int buscaIndiceFim(vector<int> vetor, int numeroProcurado, int inicio, int fim){
    if(inicio == fim){
        if(numeroProcurado == vetor[inicio]) {
            usleep(1);
            return inicio;
        }
        else
            return -1;
    }

    int meio = (inicio + fim) / 2;
    meio++;

    if(numeroProcurado >= vetor[meio]){
        return buscaIndiceFim(vetor, numeroProcurado, meio, fim);
    } else
        return buscaIndiceFim(vetor, numeroProcurado, inicio, meio - 1);
}

int elaborado(vector<int> vetor, int numeroProcurado, int inicio, int fim){
    int indiceInicio = buscaIndiceInicio(vetor, numeroProcurado, inicio, fim);

    if(indiceInicio == -1){
        return 0;
    }

    int indiceFim = buscaIndiceFim(vetor, numeroProcurado, inicio, fim);
    return indiceFim - indiceInicio + 1;
}


int main(int argc, char* argv[]) {
    cout << "Executando programa!" << endl << endl;

    if(argc != 3) {
        cout << "Informe na entrada o caminho do arquivo e número a ser encontrado\n"
                "Exemplo: ./paa <caminho-do-arquivo> <numero-a-ser-encontrado>" << endl;

        return 1;
    }

    string pathFile = argv[1];

    cout << "Abrindo arquivo " << pathFile << endl << endl;

    fstream arquivo;
    arquivo.open(pathFile.c_str());

    if(!arquivo.is_open()){
        cout << "Impossível abrir o arquivo. Verifique se o caminho do arquivo está correto." << endl;
        return 1;
    }

    string linha;
    getline(arquivo, linha);

    int tamanho = stoi(linha);

    cout << "tamanho do vetor: " << tamanho << endl;

    vector<int> vetor;


    for(int i = 0; i < tamanho; i++){
        if(!getline(arquivo, linha)){
            cout << "Parece que o arquivo não está no formato correto. Verifique!" << endl;
            arquivo.close();
            return 1;
        }
        vetor.push_back(stoi(linha));
    }

    arquivo.close();

    cout << "Ordenando..." << endl;

    sort(vetor.begin(), vetor.end());

    cout << "Ordenado!" << endl << endl;

    int numeroProcurado = stoi(argv[2]);


    cout << "Executando programa simples: procurando número " << numeroProcurado << endl;

    clock_t start = clock();
    int res = simples(vetor, tamanho, numeroProcurado);
    clock_t end = clock();
    double total = double(end - start)/100;

    cout << "O número " << numeroProcurado << " aparece " << res << " vezes." << endl;
    cout << "Algoritmo executado em " << total << " milesegundos." << endl;

    cout << endl << "Executando programa elaborado: procurando número " << numeroProcurado << endl;

    start = clock();
    res = elaborado(vetor, numeroProcurado, 0, tamanho-1);
    end = clock();
    total = double(end - start)/100;

    cout << "O número " << numeroProcurado << " aparece " << res << " vezes." << endl;
    cout << "Algoritmo executado em " << total << " milesegundos." << endl;

    return 0;
}


